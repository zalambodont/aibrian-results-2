import React from 'react';
import './App.css';
import Results from './components/Results/Results'

function App() {
  return (
    <div className="App">
      <Results />
    </div>
  );
}

export default App;

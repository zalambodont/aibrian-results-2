import React, { Component } from "react";
import { Container} from 'reactstrap';
import StarRatingComponent from 'react-star-rating-component';
import InfiniteCarousel from 'react-leaf-carousel';
import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

import 'bootstrap/dist/css/bootstrap.css';
import './Result.css';

const data =[
	{
    id: 1,
    logo: "msi.png",
    brand: "msi",
    photo: "product-image.png",
    itemPrice: "5.021.86 TL",
    itemName: "MSI GL62M Core i5",
    itemSpec: "7300HQ 2.5GHZ - 8GB - 1TB HDD - 15.6'' - GTX1050 2GB - W10",
    match: "80",
    rec: true
	},{
    id: 2,
    logo: "msi.png",
    brand: "msi",
    photo: "product-image.png",
    itemPrice: "5.021.86 TL",
    itemName: "MSI GL62M Core i5",
    itemSpec: "7300HQ 2.5GHZ - 8GB - 1TB HDD - 15.6'' - GTX1050 2GB - W10",
    match: "80",
    rec: false
	},{
    id: 3,
    logo: "msi.png",
    brand: "msi",
    photo: "product-image.png",
    itemPrice: "5.021.86 TL",
    itemName: "MSI GL62M Core i5",
    itemSpec: "7300HQ 2.5GHZ - 8GB - 1TB HDD - 15.6'' - GTX1050 2GB - W10",
    match: "80",
    rec: false
	},{
    id: 4,
    logo: "msi.png",
    brand: "msi",
    photo: "product-image.png",
    itemPrice: "5.021.86 TL",
    itemName: "MSI GL62M Core i5",
    itemSpec: "7300HQ 2.5GHZ - 8GB - 1TB HDD - 15.6'' - GTX1050 2GB - W10",
    match: "80",
    rec: false
	},{
    id: 5,
    logo: "msi.png",
    brand: "msi",
    photo: "product-image.png",
    itemPrice: "5.021.86 TL",
    itemName: "MSI GL62M Core i5",
    itemSpec: "7300HQ 2.5GHZ - 8GB - 1TB HDD - 15.6'' - GTX1050 2GB - W10",
    match: "80",
    rec: false
	},{
    id: 6,
    logo: "msi.png",
    brand: "msi",
    photo: "product-image.png",
    itemPrice: "5.021.86 TL",
    itemName: "MSI GL62M Core i5",
    itemSpec: "7300HQ 2.5GHZ - 8GB - 1TB HDD - 15.6'' - GTX1050 2GB - W10",
    match: "80",
    rec: false
	}
]


const picks=[
  {
    id: 1,
    label:'Game-cs',
    link:'/game-cs'
  },
  {
    id: 2,
    label:'Levels-expert',
    link:'/game-cs'
	},
  {
    id: 3,
    label:'Occupation-student',
    link:'/game-cs'
	},
  {
    id: 4,
    label:'Programs-software',
    link:'/game-cs'
	}
]


const reasons=[
  {
    id: 1,
    label:'Brian çok donuyor',
    link:'/game-cs'
  },
  {
    id: 2,
    label:'Sonuçlar benim ihtiyaçlarımla uygun değil',
    link:'/game-cs'
	},
  {
    id: 3,
    label:'Fiyatlar çok yüksek',
    link:'/game-cs'
	},
  {
    id: 4,
    label:'Doğru soruları sormadı',
    link:'/game-cs'
	}
]

class Results extends Component {
  constructor(props) {
    super(props);

    this.state = {
        collapseOpen: false,
        rating: 0
    }

    this.toggleCollapse = this.toggleCollapse.bind(this);

}
toggleCollapse() {
  this.setState(prevState => ({
      collapseOpen: !prevState.collapseOpen
  }));
}
onStarClick(nextValue, prevValue, name) {
  this.setState({rating: nextValue});
}
  render() {
    const { collapseOpen, rating} = this.state;

    return (
      <Container fluid>
          <div className="flex">
          <div className="brian-holder">
              <figure>
                <img src="/static/media/brian.svg" alt="Brian" />
              </figure>
                <div className='brians-picks'>
                  <span className='leading'>Brian's picks:</span>
                  <span className='message'>based on your following replies.</span>
                  <div onClick={() => this.toggleCollapse()} className="open-picks"><img src="/static/media/arrow.svg" alt="button" className={collapseOpen ? "open": null} /></div>
                  <ul className={collapseOpen ? "open": null}>
                  {
                    picks.map(item =>
                      <li key={item.id} sm="12" md="6" lg="4" xl="3">
                        <a href={item.link}>{item.label}</a>
                      </li>
                  )
                  }
                  </ul>
                </div>
            </div>
 
          <div className="result-holder">
              <h1>Computers for you</h1>
              <div className="results">
                <InfiniteCarousel
                    breakpoints={[
                      {
                        breakpoint: 600,
                        settings: {
                          slidesToShow: 1,
                          slidesToScroll: 1,
                        },
                      },{
                        breakpoint: 1200,
                        settings: {
                          slidesToShow: 2,
                          slidesToScroll: 2,
                        },
                      },{
                        breakpoint: 1600,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 3,
                        },
                      },
                    ]}
                    dots={true}
                    showSides={true}
                    sidesOpacity={.5}
                    sideSize={.1}
                    slidesToScroll={4}
                    slidesToShow={4}
                    scrollOnDevice={true}
                  >
                  {
                    data.map(item =>
                      <div key={item.id} className={item.rec ? 'result-item recommended' : 'result-item'}>
                        {item.rec ? <div className="brian-eyes"><img src="/static/media/briansec.svg" alt="BRIAN'S SPECIAL RECOMMENDATION" /></div>: null}
                        <div className="match">
                            <span className="circle">
                              <CircularProgressbar value={item.match} text={`${item.match}%`}
                                styles={{
                                  path: {
                                    stroke: `#A2DB20`,
                                    strokeLinecap: 'butt',
                                  },
                                  trail: {
                                    stroke: '#00487E',
                                    strokeLinecap: 'butt',
                                  },
                                  // Customize the text
                                  text: {
                                    // Text color
                                    fill: '#00487E',
                                    // Text size
                                    fontSize: '22px',
                                  },
                                }}
                            />
                          </span>
                          {item.rec ? <span className="rec-mark">BRIAN'S SPECIAL RECOMMENDATION</span>: null}
                          </div>
                          <div className="image-holder">
                          <figure className="product-image"><img src={"/static/media/" + item.photo} alt={item.itemName} /></figure>
                        </div>
                        <div className="specs">
                          <strong>{item.itemName}</strong><br/>
                          {item.itemSpec}
                        </div>
                        <div className="item-price">
                          {item.itemPrice}
                        </div>
                      </div>
                  )
                      }
                </InfiniteCarousel>

              
 
              </div>
              <div className={rating !== 0 ? "open satisfied-results": "satisfied-results"}>
                <h2>Are you satisfied with the result?</h2>
                <div className='rating'>
                  <StarRatingComponent 
                    name="rate1" 
                    starCount={5}
                    value={rating}
                    onStarClick={this.onStarClick.bind(this)}
                  />
                </div>
                  <ul className={rating !== 0 ? "open": null}>
                  {
                    reasons.map(item =>
                      <li key={item.id} sm="12" md="6" lg="4" xl="3">
                        <a href={item.link}>{item.label}</a>
                      </li>
                  )
                  }
                  </ul>
              </div>
            </div>
          </div>
  
      </Container>
      
    );
  }
}

export default Results;